<?php

use yii\db\Migration;

/**
 * Class m190303_173115_gen_password_hash
 */
class m190303_173115_gen_password_hash extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('users', [
            'password_hash' => \Yii::$app->security->generatePasswordHash('123456'),
        ],[
            'id'=>1
        ]);
        $this->update('users', [
            'password_hash' => \Yii::$app->security->generatePasswordHash('123456'),
        ],[
            'id'=>2
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update('users', [
            'password_hash' => '123',
        ],[
            'id'=>1
        ]);
        $this->update('users', [
            'password_hash' => '123',
        ],[
            'id'=>2
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190303_173115_gen_password_hash cannot be reverted.\n";

        return false;
    }
    */
}
