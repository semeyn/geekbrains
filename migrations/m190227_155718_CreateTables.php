<?php

use yii\db\Migration;

/**
 * Class m190227_155718_CreateTables
 */
class m190227_155718_CreateTables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('activity', [
            'id' => $this->primaryKey(),
            'date_created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'date_start' => $this->dateTime()->notNull(),
            'date_end' => $this->dateTime(),
            'title' => $this->string(150)->notNull(),
            'description' => $this->text(),
            'is_blocked' => $this->boolean()->notNull()->defaultValue(0),
            'is_repeated' => $this->boolean()->notNull()->defaultValue(0),
            'repeat_interval' => $this->integer(),
        ]);

        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'date_created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'email' => $this->string(150)->notNull(),
            'password_hash' => $this->string(300)->notNull(),
            'token' => $this->string(150),
            'fio' => $this->string(150),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('activity');
        $this->dropTable('users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190227_155718_CreateTables cannot be reverted.\n";

        return false;
    }
    */
}
