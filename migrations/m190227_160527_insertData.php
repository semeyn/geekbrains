<?php

use yii\db\Migration;

/**
 * Class m190227_160527_insertData
 */
class m190227_160527_insertData extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('users', [
            'id' => 1,
            'email' => 'email1@email.ru',
            'password_hash' => '123',
            'fio' => 'f i o',
        ]);

        $this->insert('users', [
            'id' => 2,
            'email' => 'email2@email.ru',
            'password_hash' => '123',
            'fio' => 'f2 i2 o2',
        ]);

        $this->batchInsert('activity',
            ['user_id', 'date_start', 'title'], [
                [1, date('Y-m-d H:i:s'), 'Заголовок1'],
                [1, date('Y-m-d H:i:s'), 'Заголовок1_1'],
                [1, '2018-12-01', 'Заголовок1_2'],
                [1, date('Y-m-d H:i:s'), 'Заголовок1_3'],
                [2, '2018-12-02 20:20:20', 'Заголовок2_1'],
                [2, date('Y-m-d H:i:s'), 'Заголовок2_2']
            ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //$this->truncateTable('users');
        //$this->truncateTable('activity');
        $this->delete('users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190227_160527_insertData cannot be reverted.\n";

        return false;
    }
    */
}
