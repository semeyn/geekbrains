<?php

use yii\db\Migration;

/**
 * Class m190311_044050_add_notification
 */
class m190311_044050_add_notification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('activity', 'use_notification', $this->boolean()->notNull()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('activity','use_notification');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190311_044050_add_notification cannot be reverted.\n";

        return false;
    }
    */
}
