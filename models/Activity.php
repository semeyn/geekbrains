<?php

namespace app\models;


/**
 * This is the model class for table "activity".
 *
 * @property int $id
 * @property string $date_created
 * @property string $date_start
 * @property string $date_end
 * @property string $title
 * @property string $description
 * @property int $is_blocked
 * @property int $is_repeated
 * @property int $repeat_interval
 * @property int $user_id
 * @property int $use_notification
 *
 * @property Users $user
 */
class Activity extends ActivityBase
{
    public function rules()
    {
        return array_merge([
            [['date_start'], 'required'],
            ['date_start', 'date',  'format' => 'php:Y-m-d H:i:s'],
            ['date_end', 'date',  'format' => 'php:Y-m-d H:i:s'],
        ], parent::rules());
    }
}
