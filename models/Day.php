<?php

namespace app\models;

use yii\base\Model;

/**
* Day класс
* 
* Отражает сущность дня
*/
class Day extends Model
{
    /**
    * Дата дня
    *
    * @var int 
    */
    public $dateDay;    

    /**
    * Рабочий или выходной день
    *
    * @var boolean
    */
    public $isWorkingDay;

    public function attributeLabels()
    {
        return [
            'dateDay' => 'Дата',
            'isWorkingDay' => 'Рабочий день'            
        ];
    }
    
    public function rules() {
        return [             
            [['dateDay'], 'required'],
            ['dateDay','int'],
            ['isWorkingDay','boolean'],
        ];
    }
}