<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property int $id
 * @property string $date_created
 * @property string $date_start
 * @property string $date_end
 * @property string $title
 * @property string $description
 * @property int $is_blocked
 * @property int $is_repeated
 * @property int $repeat_interval
 * @property int $user_id
 * @property int $use_notification
 *
 * @property Users $user
 */
class ActivityBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'activity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['date_created', 'date_start', 'date_end'], 'safe'],
            [['date_start', 'title', 'user_id'], 'required'],
            [['description'], 'string'],
            [['is_blocked', 'is_repeated', 'repeat_interval', 'user_id', 'use_notification'], 'integer'],
            [['title'], 'string', 'max' => 150],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Users::class,
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_end' => Yii::t('app', 'Date End'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'is_blocked' => Yii::t('app', 'Is Blocked'),
            'is_repeated' => Yii::t('app', 'Is Repeated'),
            'repeat_interval' => Yii::t('app', 'Repeat Interval'),
            'user_id' => Yii::t('app', 'User ID'),
            'use_notification' => Yii::t('app', 'Use Notification'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }
}