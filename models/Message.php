<?php

namespace app\models;

use yii\base\Model;

/**
* Day класс
* 
* Отражает сущность дня
*/
class Message extends Model
{
    /**
    *
    * @var string
    */
    public $title;

    /**
    *
    * @var string
    */
    public $body;

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок',
            'body' => 'Текст'
        ];
    }
    
    public function rules() {
        return [             
            [['title','body'], 'required'],
        ];
    }
}