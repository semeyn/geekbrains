<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06.03.19
 * Time: 22:16
 */

namespace app\models;


use app\components\UsersComponent;
use yii\data\ActiveDataProvider;

class ActivitySearch extends Activity
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['date_created', 'date_start', 'date_end', 'title', 'description','user_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Activity::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Activity::find()->cache(10);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
        ]);

        $query->andFilterWhere(['like', 'date_start', $this->date_start])
            ->andFilterWhere(['like', 'date_end', $this->date_end])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'email', $this->user_id])
            ->join('LEFT JOIN', 'users', 'user_id = users.id');

        //\Yii::info("Искомая переменная ". $this->user_id. "|" . print_r());

        /** Если пользователь с ролью администратора
         * грузим все активности
         */
        if (!\Yii::$app->rbac->currentUserRoleIs('admin')) {
            $query->andWhere(['user_id' => \Yii::$app->user->id]);
        }

        return $dataProvider;
    }
}