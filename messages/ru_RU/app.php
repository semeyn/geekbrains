<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 13.03.19
 * Time: 23:48
 */

return [
    'My calendar' => 'Мой календарь',
    'Home' => 'Главная',
    'Calendar' => 'Календарь',
    'Activity' => 'События',
    'Cabinet' => 'Личный кабинет',
    'Title' => 'Заголовок',
    'Date Created' => 'Дата создания',
    'Date Start' => 'Дата начала',
    'Date End' => 'Дата окончания',
    'Description' => 'Описание',
    'Is Blocked' => 'Блокировать остальные события',
    'Is Repeated' => 'Повторять событие',
    'Repeat Interval' => 'Интервал повтора',
    'User ID' => '№ пользователя',
    'ID' => '№',
    'Use Notification' => 'Уведомлять',
    'The beginning and end of the event' => 'Начало и окончание события',
    'Email' => 'E-mail',
    'Fio' => 'Фамилия Имя Отчество',
    'Create Users' => 'Создать пользователя',
    'Save' => 'Сохранить',
    'Edit' => 'Редактировать',
    'Editing' => 'Редактирование',
    'Delete' => 'Удалить',
    'Users' => 'Пользователи',
    'Password' => 'Пароль',
    'Password Repeat' => 'Повторный Пароль',
    'Edit Users:' => 'Редактирование пользователя: ',
    'New Activity' => 'Новое событие',
    'Please fill in the fields:' => 'Пожалуйста, заполните поля:',
    'Enter title' => 'Введите заголовок',
    'Select the start date of the event' => 'Выберите дату начала события',
    'Select the end date of the event' => 'Выберите дату окончания события',
    'Choose one of the options' => 'Выберите один из вариантов'
];