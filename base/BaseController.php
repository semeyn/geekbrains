<?php
namespace app\base;
use yii\web\Controller;

class BaseController extends Controller
{    
    public function afterAction($action, $result)
    {
        /**установка flash-сообщения с названием "lastPage" для
         *  последующего вывода в footer главного шаблона */
        $session = \Yii::$app->session;
        $session->setFlash("lastPage", \Yii::$app->request->absoluteUrl);
        
        return parent::afterAction($action,$result);
    }
   
}