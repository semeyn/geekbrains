<?php

namespace app\controllers;

use app\base\BaseController;
use app\controllers\actions\ActivityCreateAction;
use app\controllers\actions\ActivityDeleteAction;
use app\controllers\actions\ActivityUpdateAction;
use app\controllers\actions\ActivityViewAction;
use app\controllers\actions\ActivityIndexAction;

class ActivityController extends BaseController
{
    /**
     * Действие контроллера по умолчанию
     *
     * @var string
     */
    public $defaultAction = 'index';

    public function actions()
    {
        return [
            'index' => ['class' => ActivityIndexAction::class,],
            'create' => ['class' => ActivityCreateAction::class,],
            'view' => ['class' => ActivityViewAction::class,],
            'delete' => ['class' => ActivityDeleteAction::class,],
            'update' => ['class' => ActivityUpdateAction::class,],
        ];
    }
}