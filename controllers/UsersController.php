<?php

namespace app\controllers;

use app\base\BaseController;
use app\controllers\actions\UsersCreateAction;
use app\controllers\actions\UsersDeleteAction;
use app\controllers\actions\UsersUpdateAction;
use app\controllers\actions\UsersIndexAction;
use app\controllers\actions\UsersViewAction;
use app\controllers\actions\UsersInfoAction;
use app\controllers\actions\UsersEditAction;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => ['class' => UsersIndexAction::class,],
            'create' => ['class' => UsersCreateAction::class,],
            'view' => ['class' => UsersViewAction::class,],
            'update' => ['class' => UsersUpdateAction::class,],
            'delete' => ['class' => UsersDeleteAction::class,],
            'info' => ['class' => UsersInfoAction::class,],
            'edit' => ['class' => UsersEditAction::class,],
        ];
    }
}
