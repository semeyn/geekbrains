<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03.03.19
 * Time: 13:20
 */

namespace app\controllers;

use yii\web\Controller;
use app\controllers\actions\AuthSignInAction;
use app\controllers\actions\AuthSignUpAction;
use app\controllers\actions\AuthSignOutAction;

class AuthController extends Controller
{

    /**
     * Действие контроллера по умолчанию
     *
     * @var string
     */
    public $defaultAction = 'sign-in';

    public function actions()
    {
        return [
            'sign-in' => ['class' => AuthSignInAction::class,],
            'sign-up' => ['class' => AuthSignUpAction::class,],
            'sign-out' => ['class' => AuthSignOutAction::class,],
        ];
    }
}