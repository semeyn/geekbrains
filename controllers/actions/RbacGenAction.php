<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 04.03.19
 * Time: 22:08
 */

namespace app\controllers\actions;
use yii\base\Action;

class RbacGenAction extends Action
{
    public function run()
    {
        \Yii::$app->rbac->generateRbacRules();
    }
}
