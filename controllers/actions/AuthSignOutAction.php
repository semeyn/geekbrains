<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03.03.19
 * Time: 20:35
 */

namespace app\controllers\actions;

use Yii;
use yii\base\Action;

class AuthSignOutAction extends Action
{
    public function run()
    {
        Yii::$app->user->logout();

        return $this->controller->goHome();
    }

}