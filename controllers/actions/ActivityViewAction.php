<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24.02.19
 * Time: 1:44
 */

namespace app\controllers\actions;

use app\components\ActivityComponent;
use yii\base\Action;
use yii\web\HttpException;

class ActivityViewAction extends Action
{
    public function run($id)
    {
        if(!\Yii::$app->rbac->canCreateActivity()){
            throw new HttpException(403,'Нет доступа к странице');
        }

        /** @var ActivityComponent $comp */
        $comp = \Yii::$app->activity;

        $model = $comp->findModel($id);
        return $this->controller->render('view', ['model' => $model]);
    }

}