<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.03.19
 * Time: 3:03
 */

namespace app\controllers\actions;


use app\components\ActivityComponent;
use Yii;
use yii\base\Action;

class ActivityDeleteAction extends Action
{
    /**
     * @param $id
     * @return mixed
     */
    public function run($id)
    {
        /** @var ActivityComponent $comp */
        $comp = Yii::$app->activity;

        $model = $comp->findModel($id);

        $model->delete();

        Yii::$app->session->addFlash('success', 'Запись успешно удалена');
        return $this->controller->  redirect(['index']);
    }

}