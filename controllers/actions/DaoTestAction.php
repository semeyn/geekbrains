<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.02.19
 * Time: 1:24
 */

namespace app\controllers\actions;
use yii\web\HttpException;
use yii\base\Action;

class DaoTestAction extends Action
{
    public function run()
    {
        if(!\Yii::$app->rbac->canCreateActivity()){
            throw new HttpException(403,'Нет доступа к странице');
        }

        /** @var  DaoComponent $dao */
        $dao = \Yii::$app->dao;
        $users = $dao->getAllUsers();
        $activity = $dao->getAllActivity();
        $activityUser = $dao->getActivityUser();
        $firstActivity = $dao->getFirstActivity(2);
        return $this->controller->render('test', ['users' => $users, 'activity' => $activity, 'activityUser' => $activityUser, 'firstActivity' => $firstActivity]);
    }
}