<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 23.02.19
 * Time: 22:08
 */

namespace app\controllers\actions;

use app\models\Activity;
use yii\base\Action;
use yii\web\HttpException;
use app\components\ActivityComponent;

class ActivityCreateAction extends Action
{
    /**
     * @return string
     * @throws HttpException
     */
    public function run()
    {
        if (!\Yii::$app->rbac->canCreateActivity()) {
            throw new HttpException(403, 'Нет доступа к странице');
        }

        /** @var ActivityComponent $comp */
        $comp = \Yii::$app->activity;
        $request = \Yii::$app->request;

        $model = $comp->getModel($request->post());

        if ($request->isPost) {
            if ($comp->createNewActivity($model)) {

                \Yii::$app->session->addFlash('success', 'Запись успешно добавлена');
                return $this->controller->redirect(['/activity/create']);
            }
        }
        return $this->controller->render('create', ['model' => $model]);
    }
}
