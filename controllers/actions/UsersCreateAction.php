<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.03.19
 * Time: 1:39
 */

namespace app\controllers\actions;


use app\components\UsersComponent;
use app\models\Users;
use yii\base\Action;
use yii\web\HttpException;

class UsersCreateAction extends Action
{
    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws HttpException
     */
    public function run()
    {
        if (!\Yii::$app->rbac->canCreateUsers()) {
            throw new HttpException(403, 'Нет доступа к странице');
        }

        /** @var UsersComponent $comp */
        $comp = \Yii::$app->users;
        $request = \Yii::$app->request;

        /** @var Users $model */
        $model = $comp->getModel($request->post());

        if ($request->isPost) {
            if ($comp->createNewUser($model)) {
                \Yii::$app->session->addFlash('success', 'Запись успешно добавлена');
                return $this->controller->redirect(['view', 'id' => $model->id]);
            } else {
                \Yii::$app->session->addFlash('error', 'Ошибка добавления записи');
            }
        }
        return $this->controller->render('create', [
            'model' => $model
        ]);
    }
}