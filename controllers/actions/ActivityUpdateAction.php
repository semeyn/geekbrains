<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.03.19
 * Time: 2:58
 */

namespace app\controllers\actions;


use app\components\ActivityComponent;
use Yii;
use yii\base\Action;

class ActivityUpdateAction extends Action
{

    /**
     * @param $id
     * @return string
     */
    public function run($id)
    {
        /** @var ActivityComponent $comp */
        $comp = Yii::$app->activity;

        $model = $comp->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) {
                \Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
            } else {
                \Yii::$app->session->addFlash('error', 'Ошибка обновления записи');
            }
            return $this->controller->redirect(['view', 'id' => $model->id]);
        }

        return $this->controller->render('update', [
            'model' => $model,
        ]);
    }


}