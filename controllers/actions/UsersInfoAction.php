<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.03.19
 * Time: 1:27
 */

namespace app\controllers\actions;


use app\components\UsersComponent;
use yii\base\Action;

class UsersInfoAction extends Action
{
    /**
     * Displays a single Users model.
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function run() {
        /** @var UsersComponent $comp */
        $comp = \Yii::$app->users;


        $model = $comp->findModel(\Yii::$app->user->id);
        return $this->controller->render('info', [
            'model' => $model,
        ]);
    }
}