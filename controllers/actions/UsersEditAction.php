<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.03.19
 * Time: 2:58
 */

namespace app\controllers\actions;


use app\components\UsersComponent;
use Yii;
use yii\base\Action;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class UsersEditAction extends Action
{

    /**
     * @var
     */
    public $password;

    /**
     * @var
     */
    public $repeat_password;

    /**
     * @param $id
     * @return string
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function run()
    {
        /** @var UsersComponent $comp */
        $comp = Yii::$app->users;

        $model = $comp->findModel(\Yii::$app->user->id);

        $model->password = $model->password_hash;
        $model->password_repeat = $model->password_hash;

        if ($model->load(Yii::$app->request->post())) {
            if($model->password != $model->password_hash) {
                $model->password_hash = $comp->hashPassword($model->password);
            }
            if($model->save()) {
                \Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
            } else {
                \Yii::$app->session->addFlash('error', 'Ошибка обновления записи');
            }
            return $this->controller->redirect(['info']);
        }

        return $this->controller->render('edit', [
            'model' => $model,
        ]);
    }


}