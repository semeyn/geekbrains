<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03.03.19
 * Time: 20:35
 */

namespace app\controllers\actions;

use yii\base\Action;
use app\components\UsersAuthComponent;

class AuthSignInAction extends Action
{
    public function run()
    {
        /** @var UsersAuthComponent $comp */
        $comp = \Yii::$app->auth;

        $model = $comp->getModel(\Yii::$app->request->post());

        if (\Yii::$app->request->isPost) {
            if ($comp->loginUser($model)) {
                return $this->controller->redirect(['/activity']);
            }
        }

        return $this->controller->render('signin', ['model' => $model]);

    }

}