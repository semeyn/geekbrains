<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.03.19
 * Time: 1:24
 */

namespace app\controllers\actions;

use app\models\UsersSearch;
use yii\base\Action;
use yii\web\HttpException;

class UsersIndexAction extends Action
{
    /**
     * Lists all Users models.
     * @return mixed
     * @throws HttpException
     */

    public function run() {

        if (!\Yii::$app->rbac->canViewUsers()) {
            throw new HttpException(403, \Yii::t('error','No access to page'));
        }

        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->controller->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}