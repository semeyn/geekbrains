<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24.02.19
 * Time: 1:44
 */

namespace app\controllers\actions;


use app\models\ActivitySearch;
use yii\base\Action;
use yii\web\HttpException;
use app\components\ActivityComponent;

class ActivityIndexAction extends Action
{
    /**
     * @throws HttpException
     */
    public function run() {

        if (!\Yii::$app->rbac->canCreateActivity()) {
            throw new HttpException(403, \Yii::t('error','No access to page'));
        }

        $searchModel = new ActivitySearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->controller->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}