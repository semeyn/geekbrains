<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.03.19
 * Time: 3:03
 */

namespace app\controllers\actions;


use app\components\UsersComponent;
use Yii;
use yii\base\Action;

class UsersDeleteAction extends Action
{
    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if (!\Yii::$app->rbac->canViewUsers()) {
            throw new HttpException(403, 'Нет доступа к странице');
        }

        /** @var UsersComponent $comp */
        $comp = Yii::$app->users;

        $model = $comp->findModel($id);

        $model->delete();

        Yii::$app->session->addFlash('success', 'Запись успешно удалена');
        return $this->controller->  redirect(['index']);
    }

}