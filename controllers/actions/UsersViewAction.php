<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.03.19
 * Time: 1:27
 */

namespace app\controllers\actions;


use app\components\UsersComponent;
use yii\base\Action;

class UsersViewAction extends Action
{
    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id) {
        /** @var UsersComponent $comp */
        $comp = \Yii::$app->users;

        $model = $comp->findModel($id);
        return $this->controller->render('view', [
            'model' => $model,
        ]);
    }
}