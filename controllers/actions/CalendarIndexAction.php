<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11.03.19
 * Time: 23:03
 */

namespace app\controllers\actions;


use app\components\CalendarComponent;
use yii\base\Action;

class CalendarIndexAction extends Action
{
    /**
     * @return string
     */
    public function run() {

        /** @var CalendarComponent $comp */
        $comp = \Yii::$app->calendar;
        $events = $comp->getTestEvents();
        return $this->controller->render('index', [
            'events' => $events]);
    }
}