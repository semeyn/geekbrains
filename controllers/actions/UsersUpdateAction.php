<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.03.19
 * Time: 2:58
 */

namespace app\controllers\actions;


use app\components\UsersComponent;
use Yii;
use yii\base\Action;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class UsersUpdateAction extends Action
{

    /**
     * @var
     */
    public $password;

    /**
     * @var
     */
    public $repeat_password;

    /**
     * @param $id
     * @return string
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function run($id)
    {
        /** @var UsersComponent $comp */
        $comp = Yii::$app->users;

        $model = $comp->findModel($id);

        $model->password = $model->password_hash;
        $model->password_repeat = $model->password_hash;

        if ($model->load(Yii::$app->request->post())) {
            if($model->password != $model->password_hash) {
                $model->password_hash = $comp->hashPassword($model->password);
            }
            if($model->save()) {
                \Yii::$app->session->addFlash('success', 'Запись успешно обновлена');
            } else {
                \Yii::$app->session->addFlash('error', 'Ошибка обновления записи');
            }
            return $this->controller->redirect(['view', 'id' => $model->id]);
            //return $this->controller->redirect(Yii::$app->session->getFlash('lastPage') ?: Yii::$app->homeUrl);
        }

        return $this->controller->render('update', [
            'model' => $model,
        ]);
    }


}