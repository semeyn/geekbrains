<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03.03.19
 * Time: 20:38
 */

namespace app\controllers\actions;

use yii\base\Action;
use app\components\UsersAuthComponent;

class AuthSignUpAction extends Action
{
    public function run()
    {
        /** @var UsersAuthComponent $comp */
        $comp = \Yii::$app->auth;
        $request = \Yii::$app->request;

        $model = $comp->getModel($request->post());

        if ($request->isPost) {
            if ($comp->createNewUser($model)) {

                \Yii::$app->session->addFlash('success', 'Пользователь успешно добавлен ID ' . $model->id);
                return $this->controller->redirect(['/']);

            }
        }
        return $this->controller->render('signup', ['model' => $model]);
    }

}