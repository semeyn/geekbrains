<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.02.19
 * Time: 1:14
 */

namespace app\controllers;


use app\base\BaseController;
use app\controllers\actions\DaoTestAction;

class DaoController extends BaseController
{
    /**
     * Действие контроллера по умолчанию
     *
     * @var string
     */
    public $defaultAction = 'test';

    public function actions()
    {
        return [
            'test' => ['class' => DaoTestAction::class,],
        ];
    }
}