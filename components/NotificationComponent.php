<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11.03.19
 * Time: 13:22
 */

namespace app\components;

use app\models\Activity;
use yii\base\Component;
use yii\mail\MailerInterface;

class NotificationComponent extends Component
{
    /** @var MailerInterface */
    public $mailer;

    /**
     * @param $activities Activity[]
     * @return \Generator
     */
    public function sendTodayNotification($activities) {
        foreach ($activities as $activity) {
            $result = $this->mailer->compose('notification', ['model' => $activity])
                ->setFrom('semeneynikov@yandex.ru')
                //Почта пользователя $activity->user->email не существует, для теста шлем на свою
                ->setTo('semeneynikov@gmail.com')
                ->setSubject('Событие сегодня')
                ->attach(\Yii::getAlias('@app/web/files/47801551032942.png'))
                ->send();

            yield['result' => $result, 'email' => $activity->user->email];
        }
    }

    public function sendMessage($email, $message) {
        $result = $this->mailer->compose('message', ['model' => $message])
            ->setFrom('semeneynikov@yandex.ru')
            //Почта пользователя $activity->user->email не существует, для теста шлем на свою
            ->setTo($email)
            ->setSubject('Сообщение: ' . $message->title)
            ->send();

        return $result;

        //yield['result' => $result, 'email' => $email];
    }
}