<?php
/**
 * Created by PhpStorm.
 * User: Talisman
 * Date: 28.02.2019
 * Time: 20:26
 */

namespace app\components;

use app\rules\ViewActivityOwnerRule;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 *
 * @property \yii\rbac\ManagerInterface $authManager
 */
class RbacComponent extends Component
{
    /**
     * @return \yii\rbac\ManagerInterface
     */
    public function getAuthManager()
    {
        return \Yii::$app->authManager;
    }

    public function generateRbacRules()
    {
        $authManager = $this->getAuthManager();

        $authManager->removeAll();

        $admin = $authManager->createRole('admin');
        $user = $authManager->createRole('user');

        $authManager->add($admin);
        $authManager->add($user);

        $createActivity = $authManager->createPermission('createActivity');
        $createActivity->description = 'Создание активности';

        $viewOwnerRule = new ViewActivityOwnerRule();
        $authManager->add($viewOwnerRule);


        $viewActivity = $authManager->createPermission('viewActivity');
        $viewActivity->description = 'Просмотр активности';
        $viewActivity->ruleName = $viewOwnerRule->name;

        $viewEditAll = $authManager->createPermission('viewEditAll');
        $viewEditAll->description = 'Просмотр и редактирование всех активностей';

        $createUsers = $authManager->createPermission('createUsers');
        $createUsers->description = 'Создание пользователя';

        $viewUsers = $authManager->createPermission('viewUsers');
        $viewUsers->description = 'Просмотр и редактирование пользователя';

        $authManager->add($createActivity);
        $authManager->add($viewActivity);
        $authManager->add($viewEditAll);
        $authManager->add($createUsers);
        $authManager->add($viewUsers);

        $authManager->addChild($user, $createActivity);
        $authManager->addChild($user, $viewActivity);

        $authManager->addChild($admin, $user);
        $authManager->addChild($admin, $viewEditAll);
        $authManager->addChild($admin, $createUsers);
        $authManager->addChild($admin, $viewUsers);

        $authManager->assign($user, 2);
        $authManager->assign($admin, 1);
    }

    /**
     * @return bool
     */
    public function canCreateActivity()
    {
        return \Yii::$app->user->can('createActivity');
    }

    public function canViewEditAll()
    {
        return \Yii::$app->user->can('viewEditAll');
    }

    public function canViewActivity($activity): bool
    {
        return \Yii::$app->user->can('viewActivity', ['activity' => $activity]);
    }

    public function canCreateUsers()
    {
        return \Yii::$app->user->can('createUsers');
    }

    public function canViewUsers()
    {
        return \Yii::$app->user->can('viewUsers');
    }

    /**
     * @param $name
     * @return bool
     */
    public static function currentUserRoleIs($name)
    {
        $userRole = current(ArrayHelper::getColumn(\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->id),
            'name'));
        return $name == $userRole;
    }

    public function setUserRole($id)
    {
        $authManager = $this->getAuthManager();
        $user = $authManager->getRole('user');
        $authManager->assign($user, $id);
    }


}