<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.02.19
 * Time: 1:33
 */

namespace app\components;


use yii\base\Component;

class DaoComponent extends Component
{

    /**
     *
     * @return \yii\db\Connection
     */

    public function getDB()
    {
        return \Yii::$app->db;
    }

    public function getAllUsers()
    {
        $sql = 'select * from users';
        return $this->getDB()->createCommand($sql)->queryAll();
    }

    public function getAllActivity()
    {
        $sql = 'select * from activity';
        return $this->getDB()->createCommand($sql)->queryAll();
    }

    public function getActivityUser($id = 1)
    {
        $sql = 'select * from activity where user_id=:user';
        return $this->getDB()->createCommand($sql, [':user' => $id])->queryAll();
    }

    public function getFirstActivity($count = 1)
    {
        $sql = 'select * from activity LIMIT :count';
        return $this->getDB()->createCommand($sql, [':count' => $count])->queryAll();
    }


}