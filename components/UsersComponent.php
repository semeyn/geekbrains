<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.03.19
 * Time: 1:47
 */

namespace app\components;


use app\models\Users;
use Yii;
use yii\base\Component;
use yii\web\NotFoundHttpException;
use app\components\RbacComponent;

/**
 *
 * @property mixed $authManager
 * @property mixed $rules
 */
class UsersComponent extends Component
{
    public $newpassword;

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Проверка модели
     * @param null $params
     * @return Model
     */
    public function getModel($params = null)
    {
        /**
         * Получаем подель
         * @var model
         */
        $model = new Users();
        if ($params && is_array($params)) {
            $model->load($params);
        }
        return $model;
    }

    /**
     * @param $model Users
     * @return bool
     */
    public function createNewUser(&$model): bool
    {
        if (!$model->validate(['email', 'fio'])) {
            return false;
        }
        $newpassword = $this->GenPassword(6);
        $model->password = $newpassword;
        $model->password_repeat = $newpassword;

        $model->password_hash = $this->hashPassword($newpassword);

        if (!$model->validate()) {
            return false;
        }
        if ($model->save()) {
            $this->setRules($model->id);
            $this->sendMail(Yii::getAlias('@from'), $model->email, "Registration", "Your password: " . $newpassword);
            return true;
        }
        return false;
    }

    public function hashPassword($password)
    {
        return \Yii::$app->security->generatePasswordHash($password);
    }

    private function GenPassword($length = 10)
    {
        $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        $length = intval($length);
        $size = strlen($chars) - 1;
        $password = "";
        while ($length--) {
            $password .= $chars[rand(0, $size)];
        }
        return $password;
    }

    /**
     * @param $from
     * @param $to
     * @param $subject
     * @param $body
     */
    public function sendMail($from, $to, $subject, $body)
    {
        Yii::$app->mailer->compose()
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            //->setTextBody($body)
            ->setHtmlBody($body)
            ->send();
    }

    public function getAuthManager(){
        return \Yii::$app->authManager;
    }

    /**
     * @param $rule
     */
    public function setRules($id)
    {
        $comp = Yii::$app->rbac;
        $comp->setUserRole($id);
    }

    public function getIdUserByEmail($email) {
        $users = Users::find()->andFilterWhere(['like', 'email', $email])->all();
        return $users;
    }
}