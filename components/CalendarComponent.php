<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11.03.19
 * Time: 23:43
 */

namespace app\components;


use yii\base\Component;

/**
 *
 * @property void $testEvents
 */
class CalendarComponent extends Component
{
    public function getTestEvents() {
        $events = array();
        //Testing
        $Event = new \yii2fullcalendar\models\Event();
        $Event->id = 1;
        $Event->title = 'Testing';
        $Event->start = date('Y-m-d\Th:m:s\Z');
        $events[] = $Event;

        $Event = new \yii2fullcalendar\models\Event();
        $Event->id = 2;
        $Event->title = 'Testing';
        $Event->start = date('Y-m-d\Th:m:s\Z',strtotime('tomorrow 6am'));
        $events[] = $Event;

        return $events;
    }

}