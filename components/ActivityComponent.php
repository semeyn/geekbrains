<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24.02.19
 * Time: 2:22
 */

namespace app\components;

use app\models\Activity;
use app\models\ActivitySearch;
use yii\base\Component;
use yii\i18n\Formatter;

/**
 *
 * @property mixed $allActivity
 * @property string $pathSaveFile
 */
class ActivityComponent extends Component
{
    /**
     * Имя класса
     * @var  string
     */
    public $activity_class;

//    /**
//     * Проверка модели
//     * @param null $params
//     * @return Model
//     */
//    public function getModel($params = null) {
//        /**
//         * Получаем подель
//         * @var model
//         */
//        $model = new $this->activity_class;
//        if ($params && is_array($params)) {
//            $model->load($params);
//        }
//        return $model;
//    }

//    /**
//     * Заполняем модель тестовыми параметрами
//     * @param $id
//     * @return Model
//     */
//
//    public function CreateModel($id) {
//        $model = new $this->activity_class;
//        $model->user_id = $id;
//        $model->date_start = time();
//        $model->date_end = time();
//        return $model;
//    }

    /**
     * Загружаем файлы
     * @param $model
     */
    public function loadFile(&$model) {
        if ($model->images) {
            /** @var  $path */
            $path = $this->getPathSaveFile();
            foreach ($model->images as $key => $image) {
                $name = mt_rand(0, 9999) . time() . "." . $image->getExtension();
                $image->saveAs($path . $name);
                $model->images[$key] = $name;
            }
        }
    }

    /**
     * Получаем путь к директории  для загрузки файлов
     * @return string
     */
    public function getPathSaveFile() {
        return \Yii::getAlias('@files');
    }

    /**
     * Проверка модели
     * @param null $params
     * @return Model
     */
    public function getModel($params = null)
    {
        /**
         * Получаем подель
         * @var model
         */
        $model = new Activity();
        if ($params && is_array($params)) {
            $model->load($params);
        }
        return $model;
    }

    /**
     * @param $model Activity
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function createNewActivity(&$model): bool {
        $model->user_id = \Yii::$app->user->id;

        /** @var Formatter $formatter */
        $formatter = \Yii::$app->formatter;
        $model->date_start = $formatter->asDatetime($model->date_start, 'php:Y-m-d H:i:s');
        $model->date_end = $formatter->asDatetime($model->date_end, 'php:Y-m-d H:i:s');

        if (!$model->validate()) {
            return false;
        }

        if ($model->save()) {
            return true;
        }
        return false;
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id) {
        if (($model = Activity::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getActivity($id) {
        return Activity::find()->cache(10)->andWhere(['id' => $id])->all();
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getActivityByEmail($email) {
        return Activity::find()->cache(10)->andWhere(['email' => $email])->all();
    }

    /**
     * @return mixed
     */
    public function getAllActivity() {
        return Activity::find()->cache(10)->all();
    }

    /**
     * Получение списка сегодняшних событий
     * @return Activity[]|array|\yii\db\ActiveRecord[]
     */
    public function getActivityToday() {
        $activities = Activity::find()->cache(10)->andWhere('date_start>=:date', [':date' => date('Y-m-d')])
            ->andWhere(['use_notification' => 1])->all();
        return $activities;
    }


    /**
     * Получение списка сегоднящних событий
     * @param $params
     * @return \yii\data\ActiveDataProvider
     */
    /*
    public function getSearchProvider($params) {
        $model = new ActivitySearch();

        return $model->getDataProvider();
    }*/
}