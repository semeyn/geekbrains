<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Событие';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-view">
    <p>
        <?= Html::a(Yii::t('app','Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
        echo Html::tag('h1', "Активность: " . Html::encode($model->title));

        if ($model->date_start == $model->date_end):
            echo Html::tag('p', "Событие на " . Html::encode($model->date_start));
        else:
            echo Html::tag('p', "Событие c " . Html::encode($model->date_start) . " по " . Html::encode($model->date_end));
        endif;

        echo Html::tag('h3', $model->getAttributeLabel('description'));
        echo Html::tag('div', Html::encode($model->description));

        echo Html::tag('h3', $model->getAttributeLabel('is_blocked'));
        echo Html::tag('div', Html::encode($model->is_blocked ? 'Да' : 'Нет'));

        echo Html::tag('h3', $model->getAttributeLabel('is_repeated'));
        echo Html::tag('div', Html::encode($model->is_repeated ? 'Да' : 'Нет'));

        if ($model->repeat_interval != null) {
            echo Html::tag('h3', $model->getAttributeLabel('repeat_interval'));
            echo Html::tag('div', Html::encode($model->repeat_interval));
        }
/*
        if ($model->images) {
            echo Html::tag('h3', $model->getAttributeLabel('images'));
            foreach ($model->images as $image) {
                echo Html::img('/files/' . $image)."<br>";
            }
        }*/
    ?>
</div>