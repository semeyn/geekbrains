<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Activity */

$this->title = Yii::t('app', 'New Activity');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-create">
    <p><?=Yii::t('app','Please fill in the fields:')?></p>
    <div class="row">
        <div class="col-md-6">

            <?php $form = ActiveForm::begin([
                'method' => 'POST'
            ]); ?>

            <?= $form->field($model, 'title')->textInput(['autofocus' => true, 'placeholder'=>Yii::t('app','Enter title')]); ?>
            <?= $form->field($model, 'date_start')->widget("kartik\datetime\DateTimePicker", [
                'name' => 'date_start',
                'options' => ['placeholder' => Yii::t('app', 'Select the start date of the event')],
                'convertFormat' => true,
                'pluginOptions' => [
                    'format' => 'dd-MM-yyyy HH:i:s',
                    'todayHighlight' => true
                ]
            ])->label(Yii::t('app', 'Date Start')) ?>
            <?= $form->field($model, 'date_end')->widget("kartik\datetime\DateTimePicker", [
                'name' => 'date_end',
                'options' => ['placeholder' => Yii::t('app', 'Select the end date of the event')],
                'convertFormat' => true,
                'pluginOptions' => [
                    'format' => 'dd-MM-yyyy HH:i:s',
                    'todayHighlight' => true
                ]
            ])->label($model->getAttributeLabel('date_end')) ?>
            <?= $form->field($model, 'description')->textArea(['placeholder'=>Yii::t('app','Description')]); ?>
            <?= $form->field($model, 'use_notification')->checkbox(); ?>
            <?= $form->field($model, 'is_blocked')->checkbox(); ?>
            <?= $form->field($model, 'is_repeated')->checkbox(); ?>
            <?= $form->field($model, 'repeat_interval')->dropDownList([
                '1' => 5,
                '2' => 10,
                '3' => 15
            ],
                [
                    'prompt' => Yii::t('app', 'Choose one of the options')
                ]); ?>




            <div class="form-group">
                <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-primary', 'name' => 'submint-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>