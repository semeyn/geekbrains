<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Activity */

$this->title = 'Update Activity: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Activity', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => 'ID' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app','Editing');
?>
<div class="activity-update">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['autofocus' => true]); ?>
    <?=$form->field($model, 'date_start')->widget("kartik\datetime\DateTimePicker", [
        'name' => 'date_start',
        'options' => ['placeholder' => 'Выберите дату начала события'],
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'dd-MM-yyyy H:i:s',
            'todayHighlight' => true
        ]
    ])->label($model->getAttributeLabel('date_start')) ?>
    <?=$form->field($model, 'date_end')->widget("kartik\datetime\DateTimePicker", [
        'name' => 'date_end',
        'options' => ['placeholder' => 'Выберите дату окончания события'],
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'dd-MM-yyyy H:i:s',
            'todayHighlight' => true
        ]
    ])->label($model->getAttributeLabel('date_end')) ?>
    <?= $form->field($model, 'description')->textArea(); ?>
    <?= $form->field($model, 'use_notification')->checkbox(); ?>
    <?= $form->field($model, 'is_blocked')->checkbox(); ?>
    <?= $form->field($model, 'is_repeated')->checkbox(); ?>
    <?= $form->field($model, 'repeat_interval')->dropDownList([
        '1' => 5,
        '2' => 10,
        '3' => 15
    ],
        [
            'prompt' => 'Выберите один вариант'
        ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', ['/activity'], ['class'=>'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
