<?php

use yii\helpers\Html;

$this->title = 'Список событий';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-index">
    <p>
        <?= Html::a('Создать активность', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="row">
        <div class="col-md-12">
            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'rowOptions' => function ($model, $key, $index, $grid) {
                    $class = $index % 2 ? 'odd' : 'event';
                    return [
                        'class' => $class,
                        'index' => $index,
                        'key' => $key,
                    ];
                },
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => \yii\grid\SerialColumn::class],
                    [
                        'attribute' => 'title',
                        'value' => function ($model) {
                            return \yii\helpers\Html::a(Html::encode($model->title),
                                ['activity/view/', 'id' => $model->id]);
                        },
                        'format' => 'html'
                    ],
                    'date_created',
                    [
                        'attribute' => 'date_start',
                        'value' => function ($model) {
                            return $model->date_end ? (\Yii::$app->formatter->asTime($model->date_start,
                                    'short') . ' - ' . \Yii::$app->formatter->asTime($model->date_end,
                                    'short')) : (\Yii::$app->formatter->asTime($model->date_start, 'short'));
                        },
                        'label' => Yii::t('app', 'The beginning and end of the event')
                    ],
                    'description',
                    [
                        'attribute' => 'user_id',
                        'value' => function ($model) {
                            return $model->user->email;
                        },
                        'label' => 'Автор',
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ]
            ]); ?>
        </div>
    </div>
</div>