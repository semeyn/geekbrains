<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 03.03.19
 * Time: 13:34
 */

/* @var $this \yii\web\View */
/* @var $model \app\models\Users */
?>
<div class="container container-table">
    <div class="row vertical-center-row">
        <div class="col-md-4 col-md-offset-4">
            <legend>
                <center><h2><b>Регистрация</b></h2></center>
            </legend>
            <br>
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                'method' => 'POST',
                'fieldConfig' => [
                    'template'=> '{input}{hint}{error}'
                ],
            ]) ?>
            <?= $form->field($model, 'email')->input('email', [
                'class' => 'form-control',
                'placeholder' => 'Email'
            ]); ?>

            <?= $form->field($model, 'password')->passwordInput([
                'class' => 'form-control',
                'placeholder' => 'Password'
            ]); ?>

            <?= $form->field($model, 'password_repeat')->passwordInput([
                'class' => 'form-control',
                'placeholder' => 'Password repeat'
            ]); ?>

            <div class="form-group input-group input-group-lg col-md-8 col-md-offset-2 col-xs-12">
                <button type="submit" class="btn btn-default form-control">
                    <span class="glyphicon glyphicon-log-in"></span> Регистрация
                </button>
            </div>
            <?php \yii\bootstrap\ActiveForm::end(); ?>
        </div>
    </div>
</div>

