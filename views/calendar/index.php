<?php
/* @var $this yii\web\View */
?>
<div class="calendar-index">
    <?= \yii2fullcalendar\yii2fullcalendar::widget([
        'options' => [
            'lang' => 'ru',
        ],
        'events' => $events
    ]); ?>
</div>
