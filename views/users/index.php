<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app','Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Users'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model, $key, $index, $grid) {
            $class = $index % 2 ? 'odd' : 'event';
            return [
                'class' => $class,
                'index' => $index,
                'key' => $key,
            ];
        },
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'date_created',
            [
                'attribute' => 'email',
                'value' => function ($model) {
                    return \yii\helpers\Html::a(Html::encode($model->email),
                        ['users/view/', 'id' => $model->id]);
                },
                'format' => 'html'
            ],
            'fio',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
