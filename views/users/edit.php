<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = Yii::t('app', 'Edit Users:') . $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Cabinet'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-edit">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput([
        'class' => 'form-control',
        'placeholder' => Yii::t('app', 'Password')
    ]); ?>

    <?= $form->field($model, 'password_repeat')->passwordInput([
        'class' => 'form-control',
        'placeholder' => Yii::t('app', 'Password Repeat')
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
