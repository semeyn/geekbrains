<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = Yii::t('app', 'Cabinet');
//$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="users-view">

    <!--<h1><?= Html::encode($model->email) ?></h1>-->

    <p>
        <?= Html::a(Yii::t('app','Edit'), ['edit'], ['class' => 'btn btn-primary']) ?>
    </p>
    <!--
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date_created',
            'email:email',
            //'password_hash',
            //'token',
            'fio',
        ],
    ]) ?>-->
    <?php

    echo Html::tag('h3', $model->getAttributeLabel('date_created'));
    echo Html::tag('div', Html::encode($model->date_created));

    echo Html::tag('h3', $model->getAttributeLabel('email'));
    echo Html::tag('div', Html::encode($model->email));

    echo Html::tag('h3', $model->getAttributeLabel('fio'));
    echo Html::tag('div', Html::encode($model->fio));
    ?>

</div>
