<?php

return [
    'class' => 'yii\swiftmailer\Mailer',
    'useFileTransport' => false,
    'enableSwiftMailerLogging' => true,
    'transport' => [
        'class'=>'Swift_SmtpTransport',
        'host'=>'smtp.yandex.ru',
        'username' => 'geekbrains@onedeveloper.ru',
        'password' => 'qazWSX',
        'port' => '587',
        'encryption' => 'tls'
    ]
];
